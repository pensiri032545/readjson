import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter/services.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Users JSON',
      home: const MyHomePage(title: 'Flutter Read Users JSON'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List _users = [];

  // Fetch content from the json file
  Future<void> readJson() async {
    final String response = await rootBundle.loadString('assets/users.json');
    final data = await jsonDecode(response.toString());
    setState(() {
      _users = data;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: Column(
          children: [
            ElevatedButton(
              child: const Text('Load JSON Data'),
              onPressed: readJson,
            ),

            // Display the data loaded from sample.json
            _users.isNotEmpty
                ? Expanded(
                    child: ListView.builder(
                      itemCount: _users.length,
                      itemBuilder: (context, index) {
                        return Card(
                          margin: const EdgeInsets.all(10),
                          child: ListTile(
                            leading: CircleAvatar(
                              backgroundImage:
                                  NetworkImage(_users[index]["urlAvatar"]),
                            ),
                            title: Text(_users[index]["username"]),
                            subtitle: Text(_users[index]["email"]),
                          ),
                        );
                        /*
                        return Container(
                          child: Column(
                            children: [
                              ListTile(
                                leading: Text(_items[index]["id"]),
                                title: Text(_items[index]["name"]),
                                subtitle: Text(_items[index]["description"]),
                              ),
                              Divider(),
                            ],
                          ),
                        );
                        */
                      },
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
